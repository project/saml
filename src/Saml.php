<?php

declare(strict_types = 1);

namespace Drupal\saml;

use OneLogin\Saml2\Constants;

final class Saml {

  public const SIGNATURE_SHA256 = 'http://www.w3.org/2001/04/xmldsig-more#rsa-sha256';
  public const SIGNATURE_SHA348 = 'http://www.w3.org/2001/04/xmldsig-more#rsa-sha384';
  public const SIGNATURE_SHA512 = 'http://www.w3.org/2001/04/xmldsig-more#rsa-sha512';

  public const DIGEST_SHA256 = 'http://www.w3.org/2001/04/xmlenc#sha256';
  public const DIGEST_SHA384 = 'http://www.w3.org/2001/04/xmldsig-more#sha384';
  public const DIGEST_SHA512 = 'http://www.w3.org/2001/04/xmlenc#sha512';


  public static function getNameIdFormats(): array {
    return [
      Constants::NAMEID_EMAIL_ADDRESS => Constants::NAMEID_EMAIL_ADDRESS,
      Constants::NAMEID_X509_SUBJECT_NAME => Constants::NAMEID_X509_SUBJECT_NAME,
      Constants::NAMEID_WINDOWS_DOMAIN_QUALIFIED_NAME => Constants::NAMEID_WINDOWS_DOMAIN_QUALIFIED_NAME,
      Constants::NAMEID_UNSPECIFIED => Constants::NAMEID_UNSPECIFIED,
      Constants::NAMEID_KERBEROS => Constants::NAMEID_KERBEROS,
      Constants::NAMEID_ENTITY => Constants::NAMEID_ENTITY,
      Constants::NAMEID_TRANSIENT => Constants::NAMEID_TRANSIENT,
      Constants::NAMEID_PERSISTENT => Constants::NAMEID_PERSISTENT,
      Constants::NAMEID_ENCRYPTED => Constants::NAMEID_ENCRYPTED,
    ];
  }

  public static function getAuthnContexts(): array {
    return [
      Constants::AC_UNSPECIFIED => Constants::AC_UNSPECIFIED,
      Constants::AC_PASSWORD => Constants::AC_PASSWORD,
      Constants::AC_PASSWORD_PROTECTED => Constants::AC_PASSWORD_PROTECTED,
      Constants::AC_X509 => Constants::AC_X509,
      Constants::AC_SMARTCARD => Constants::AC_SMARTCARD,
      Constants::AC_KERBEROS => Constants::AC_KERBEROS,
      Constants::AC_WINDOWS => Constants::AC_WINDOWS,
      Constants::AC_TLS => Constants::AC_TLS,
    ];
  }

  public static function getSignatureAlgorithms(): array {
    return [
      self::SIGNATURE_SHA256 => self::SIGNATURE_SHA256,
      self::SIGNATURE_SHA348 => self::SIGNATURE_SHA348,
      self::SIGNATURE_SHA512 => self::SIGNATURE_SHA512,
    ];
  }

  public static function getDigestAlgorithms(): array {
    return [
      self::DIGEST_SHA256 => self::DIGEST_SHA256,
      self::DIGEST_SHA384 => self::DIGEST_SHA384,
      self::DIGEST_SHA512 => self::DIGEST_SHA512,
    ];
  }

}

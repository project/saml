<?php

declare(strict_types = 1);

namespace Drupal\saml\Controller;

use Drupal\Core\Cache\CacheableResponse;
use Drupal\Core\Cache\CacheableResponseInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Url;
use Drupal\saml\Entity\IdentityProviderInterface;
use Drupal\saml\Saml2\AuthFactoryInterface;
use Drupal\saml\User\SamlUserManagerInterface;
use Drupal\user\UserInterface;
use OneLogin\Saml2\Auth;
use OneLogin\Saml2\Error;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

final class ServiceProviderController implements ContainerInjectionInterface {

  protected $authFactory;

  protected $userManager;

  protected $renderer;

  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('saml.auth_factory'),
      $container->get('saml.user_manager'),
      $container->get('renderer')
    );
  }

  public function __construct(
    AuthFactoryInterface $auth_factory,
    SamlUserManagerInterface $user_manager,
    RendererInterface $renderer
  ) {
    $this->authFactory = $auth_factory;
    $this->userManager = $user_manager;
    $this->renderer = $renderer;
  }

  public function consume(
    IdentityProviderInterface $identity_provider,
    Request $request
  ): TrustedRedirectResponse {
    $auth = $this->getAuth($identity_provider);
    $account = $this->renderer->executeInRenderContext(
      new RenderContext(),
      function () use ($auth, $identity_provider): UserInterface {
        try {
          $auth->processResponse();
        }
        catch (Error $e) {
          // @todo log error
          throw new NotFoundHttpException();
        }

        if (!empty($auth->getErrors())) {
          // @todo log errors
          throw new BadRequestHttpException();
        }

        return $this->userManager->loginRegister(
          $auth->getNameId(),
          $identity_provider,
          $auth->getAttributes()
        );
      }
    );

    $relay_state = $this->getRelayState($request);
    $destination = $relay_state ? $this->buildDestination($relay_state) : NULL;

    if ($destination) {
      $response = new TrustedRedirectResponse($destination);
    }
    else {
      $response = new TrustedRedirectResponse($this->getFrontPageUrl());
    }

    $response
      ->getCacheableMetadata()
      ->setCacheContexts(['url.query_args']);
    $response->addCacheableDependency($identity_provider);
    $response->addCacheableDependency($account);

    return $response;
  }

  public function login(IdentityProviderInterface $identity_provider): TrustedRedirectResponse {
    $auth = $this->getAuth($identity_provider);
    $redirect_url = $this->renderer->executeInRenderContext(
      new RenderContext(),
      function () use ($auth): string {
        return $auth->login(
          Url::fromRoute('<front>')->setAbsolute(TRUE)->toString(),
          [],
          FALSE,
          FALSE,
          TRUE
        );
      }
    );

    $response = new TrustedRedirectResponse($redirect_url);
    $response->addCacheableDependency($identity_provider);

    return $response;
  }

  public function logout() {
    // @todo Add logout functionality later.
  }

  public function metadata(IdentityProviderInterface $identity_provider): CacheableResponseInterface {
    $content = $this
      ->getAuth($identity_provider)
      ->getSettings()
      ->getSPMetadata();

    $response = new CacheableResponse($content);
    $response->headers->set('Content-type', 'application/xml');
    $response->addCacheableDependency($identity_provider);

    return $response;
  }

  private function getAuth(IdentityProviderInterface $identity_provider): Auth {
    return $this->renderer->executeInRenderContext(
      new RenderContext(),
      function () use ($identity_provider): Auth {
        return $this->authFactory->get($identity_provider);
      }
    );
  }

  private function getRelayState(Request $request): array {
    if ($request->query->has('RelayState')) {
      return parse_url($request->query->get('RelayState')) ?: [];
    }

    if ($request->request->has('RelayState')) {
      return parse_url($request->request->get('RelayState')) ?: [];
    }

    return [];
  }

  private function buildDestination(array $relay_state): string {
    $result = $relay_state['path'];

    if (isset($relay_state['query'])) {
      $result .= '?' . $relay_state['query'];
    }

    if (isset($relay_state['fragment'])) {
      $result .= '#' . $relay_state['fragment'];
    }

    return $result;
  }

  private function getFrontPageUrl(): string {
    return $this->renderer->executeInRenderContext(
      new RenderContext(),
      function (): string {
        return Url::fromRoute('<front>')->toString();
      }
    );
  }

}

<?php

declare(strict_types = 1);

namespace Drupal\saml\EventSubscriber;

use Drupal\saml\Event\SamlLoginEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class EmailAddressAttributeSubscriber implements EventSubscriberInterface {

  public function onSamlLogin(SamlLoginEvent $event): void {
    $identity_provider = $event->getIdentityProvider();
    $email_attribute_name = $identity_provider->getEmailAttributeName();
    if (empty($email_attribute_name)) {
      return;
    }

    $attributes = $event->getAttributes();
    $email_address = $attributes[$email_attribute_name][0] ?? NULL;
    if (!$email_address) {
      return;
    }

    $account = $event->getAccount();
    $account->set('mail', $email_address);
  }

  public static function getSubscribedEvents(): array {
    return [
      SamlLoginEvent::NAME => ['onSamlLogin', 0],
    ];
  }

}

<?php

declare(strict_types = 1);

namespace Drupal\saml\Config;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\Url;
use OneLogin\Saml2\Constants;

class SamlSettings implements SamlSettingsInterface {

  protected $configFactory;

  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  public function getEntityId(): string {
    return Url::fromUserInput('/saml')->setAbsolute(TRUE)->toString();
  }

  public function getX509Certificate(): ?string {
    return $this->getConfig('x509_certificate');
  }

  public function setX509Certificate(string $x509_certificate): self {
    $this->setConfig('x509_certificate', $x509_certificate);
    return $this;
  }

  public function getPrivateKey(): ?string {
    $private_key_path = DRUPAL_ROOT . '/' . $this->getPrivateKeyPath();
    if (!file_exists($private_key_path)) {
      return NULL;
    }

    return file_get_contents($this->getConfig('private_key'));
  }

  public function getPrivateKeyPath(): ?string {
    return $this->getConfig('private_key');
  }

  public function setPrivateKeyPath(string $private_key_path): self {
    $this->setConfig('private_key', $private_key_path);
    return $this;
  }

  public function toArray(): array {
    return [
      'strict' => Settings::get('saml_strict_mode', TRUE),
      'debug' => Settings::get('saml_debug_mode', FALSE),
      'baseUrl' => Url::fromRoute('<front>')->setAbsolute(TRUE)->toString(),
      'sp' => [
        'entityId' => $this->getEntityId(),
        'assertionConsumerService' => [
          'url' => NULL,
          'binding' => Constants::BINDING_HTTP_REDIRECT,
        ],
        'singleLogoutService' => [
          'url' => NULL,
          'binding' => Constants::BINDING_HTTP_REDIRECT,
        ],
        'NameIDFormat' => NULL,
        'x509cert' => $this->getX509Certificate(),
        'privateKey' => $this->getPrivateKey(),
      ],
      'idp' => [],
      'compress' => [
        'requests' => TRUE,
        'responses' => TRUE,
      ],
      'security' => [
        'nameIdEncrypted' => NULL,
        'authnRequestsSigned' => NULL,
        'logoutRequestSigned' => NULL,
        'logoutResponseSigned' => NULL,
        'signMetadata' => FALSE,
        'wantMessagesSigned' => NULL,
        'wantAssertionsEncrypted' => NULL,
        'wantAssertionsSigned' => NULL,
        'wantNameId' => TRUE,
        'wantNameIdEncrypted' => NULL,
        'requestedAuthnContext' => TRUE,
        'wantXMLValidation' => TRUE,
        'relaxDestinationValidation' => FALSE,
        'destinationStrictlyMatches' => FALSE,
        'rejectUnsolicitedResponsesWithInResponseTo' => FALSE,
        'signatureAlgorithm' => NULL,
        'digestAlgorithm' => NULL,
        'lowercaseUrlencoding' => FALSE,
      ],
    ];
  }

  protected function getConfig(string $config_key) {
    return $this->configFactory
      ->get('saml.settings')
      ->get($config_key);
  }

  protected function setConfig(string $config_key, $config_value): self {
    $this->configFactory
      ->getEditable('saml.settings')
      ->set($config_key, $config_value)
      ->save();

    return $this;
  }

}

<?php

declare(strict_types = 1);

namespace Drupal\saml\Config;

interface SamlSettingsInterface {

  public function getEntityId(): string;

  public function getX509Certificate(): ?string;

  public function setX509Certificate(string $x509_certificate): self;

  public function getPrivateKey(): ?string;

  public function getPrivateKeyPath(): ?string;

  public function setPrivateKeyPath(string $private_key_path): self;

  public function toArray(): array;

}

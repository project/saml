<?php

declare(strict_types = 1);

namespace Drupal\saml\Entity;

interface IdentityProviderInterface {

  public function getEntityId(): ?string;

  public function setEntityId(string $entity_id): self;

  public function getNameIdFormat():? string;

  public function setNameIdFormat(string $name_id_format): self;

  public function getX509Certificate(): ?string;

  public function setX509Certificate(string $x509_certificate): self;

  public function getSsoUrl(): ?string;

  public function setSsoUrl(string $sso_url): self;

  public function getSsoBinding(): ?string;

  public function setSsoBinding(string $sso_binding): self;

  public function getSloRequestUrl(): ?string;

  public function setSloRequestUrl(string $slo_request_url): self;

  public function getSloResponseUrl(): ?string;

  public function setSloResponseUrl(string $slo_response_url): self;

  public function getSloBinding(): ?string;

  public function setSloBinding(string $slo_binding): self;

  public function shouldEncryptNameId(): bool;

  public function setEncryptNameId(bool $encrypt_name_id): self;

  public function shouldSignAuthnRequest(): bool;

  public function setSignAuthnRequest(bool $sign_authn_request): self;

  public function shouldSignLogoutRequest(): bool;

  public function setSignLogoutRequest(bool $sign_logout_request): self;

  public function shouldSignLogoutResponse(): bool;

  public function setSignLogoutResponse(bool $sign_logout_response): self;

  public function wantsSignedMessages(): bool;

  public function setSignedMessages(bool $signed_messages): self;

  public function wantsEncryptedAssertions(): bool;

  public function setEncryptedAssertions(bool $encrypted_assertions): self;

  public function wantsSignedAssertions(): bool;

  public function setSignedAssertions(bool $signed_assertions): self;

  public function wantsEncryptedNameId(): bool;

  public function setEncryptedNameId(bool $encrypted_name_id): self;

  public function getAuthnContext(): ?string;

  public function setAuthnContext(string $authn_context): self;

  public function getSignatureAlgorithm(): ?string;

  public function setSignatureAlgorithm(string $signature_algorithm): self;

  public function getDigestAlgorithm(): ?string;

  public function setDigestAlgorithm(string $digest_algorithm): self;

  public function isAdfsCompatible(): bool;

  public function setAdfsCompatible(bool $is_adfs_compatible): self;

  public function toSettingsArray(): array;

}

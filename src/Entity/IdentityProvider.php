<?php

declare(strict_types = 1);

namespace Drupal\saml\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Url;
use OneLogin\Saml2\Constants;

/**
 * @ConfigEntityType(
 *   id = "saml_identity_provider",
 *   label = @Translation("Identity Provider"),
 *   label_collection = @Translation("Identity Providers"),
 *   label_singular = @Translation("Identity Provider"),
 *   label_plural = @Translation("Identity Providers"),
 *   label_count = @PluralTranslation(
 *     singular = @Translation("@count Identity Provider"),
 *     plural = @Translation("@count Identity Providers"),
 *   ),
 *   handlers = {
 *     "form" = {
 *       "default" = "Drupal\saml\Form\IdentityProviderForm",
 *       "add" = "Drupal\saml\Form\IdentityProviderForm",
 *       "edit" = "Drupal\saml\Form\IdentityProviderForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider"
 *     },
 *     "list_builder" = "Drupal\saml\Entity\IdentityProviderListBuilder",
 *   },
 *   admin_permission = "administer saml",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   links = {
 *     "add-form" = "/admin/config/system/saml/identity-provider/add",
 *     "delete-form" = "/admin/config/system/saml/identity-provider/{saml_identity_provider}/delete",
 *     "edit-form" = "/admin/config/system/saml/identity-provider/{saml_identity_provider}/edit",
 *     "collection" = "/admin/config/system/saml/identity-provider",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "entity_id",
 *     "email_attribute",
 *     "name_id_format",
 *     "x509_certificate",
 *     "sso_url",
 *     "sso_binding",
 *     "slo_request_url",
 *     "slo_response_url",
 *     "slo_binding",
 *     "name_id_encrypted",
 *     "authn_request_signed",
 *     "sign_authn_request",
 *     "logout_request_signed",
 *     "logout_response_signed",
 *     "want_messages_signed",
 *     "want_assertions_encrypted",
 *     "want_assertions_signed",
 *     "want_name_id_encrypted",
 *     "authn_context",
 *     "signature_algorithm",
 *     "digest_algorithm",
 *     "adfs"
 *   }
 * )
 */
class IdentityProvider extends ConfigEntityBase implements IdentityProviderInterface {

  protected $id;

  protected $label;

  protected $entity_id;

  protected $email_attribute;

  protected $name_id_format;

  protected $x509_certificate;

  protected $sso_url;

  protected $sso_binding;

  protected $slo_request_url;

  protected $slo_response_url;

  protected $slo_binding;

  protected $name_id_encrypted;

  protected $authn_request_signed;

  protected $sign_authn_request;

  protected $logout_request_signed;

  protected $logout_response_signed;

  protected $want_messages_signed;

  protected $want_assertions_encrypted;

  protected $want_assertions_signed;

  protected $want_name_id_encrypted;

  protected $authn_context;

  protected $signature_algorithm;

  protected $digest_algorithm;

  protected $adfs;

  public function getEntityId(): ?string {
    return $this->entity_id;
  }

  public function setEntityId(string $entity_id): IdentityProviderInterface {
    $this->entity_id = $entity_id;
    return $this;
  }

  public function getEmailAttributeName(): ?string {
    return $this->email_attribute;
  }

  public function setEmailAttributeName(string $email_attribute): IdentityProviderInterface {
    $this->email_attribute = $email_attribute;
    return $this;
  }

  public function getNameIdFormat(): ?string {
    return $this->name_id_format;
  }

  public function setNameIdFormat(string $name_id_format): IdentityProviderInterface {
    $this->name_id_format = $name_id_format;
    return $this;
  }

  public function getX509Certificate(): ?string {
    return $this->x509_certificate;
  }

  public function setX509Certificate(string $x509_certificate): IdentityProviderInterface {
    $this->x509_certificate = $x509_certificate;
    return $this;
  }

  public function getSsoUrl(): ?string {
    return $this->sso_url;
  }

  public function setSsoUrl(string $sso_url): IdentityProviderInterface {
    $this->sso_url = $sso_url;
    return $this;
  }

  public function getSsoBinding(): ?string {
    if (!$this->sso_binding) {
      $this->sso_binding = Constants::BINDING_HTTP_REDIRECT;
    }

    return $this->sso_binding;
  }

  public function setSsoBinding(string $sso_binding): IdentityProviderInterface {
    $this->sso_binding = $sso_binding;
    return $this;
  }

  public function getSloRequestUrl(): ?string {
    return $this->slo_request_url;
  }

  public function setSloRequestUrl(string $slo_request_url): IdentityProviderInterface {
    return $this->slo_request_url = $slo_request_url;
  }

  public function getSloResponseUrl(): ?string {
    if (!$this->slo_response_url) {
      return $this->getSloRequestUrl();
    }

    return $this->slo_response_url;
  }

  public function setSloResponseUrl(string $slo_response_url): IdentityProviderInterface {
    $this->slo_response_url = $slo_response_url;
  }

  public function getSloBinding(): ?string {
    if (!$this->slo_binding) {
      $this->slo_binding = Constants::BINDING_HTTP_REDIRECT;
    }

    return $this->slo_binding;
  }

  public function setSloBinding(string $slo_binding): IdentityProviderInterface {
    $this->slo_binding = $slo_binding;
    return $this;
  }


  public function shouldEncryptNameId(): bool {
    return (bool) $this->name_id_encrypted;
  }

  public function setEncryptNameId(bool $encrypt_name_id): IdentityProviderInterface {
    $this->name_id_encrypted = $encrypt_name_id;
    return $this;
  }

  public function shouldSignAuthnRequest(): bool {
    return (bool) $this->authn_request_signed;
  }

  public function setSignAuthnRequest(bool $sign_authn_request): IdentityProviderInterface {
    $this->authn_request_signed = $sign_authn_request;
    return $this;
  }

  public function shouldSignLogoutRequest(): bool {
    return (bool) $this->logout_request_signed;
  }

  public function setSignLogoutRequest(bool $sign_logout_request): IdentityProviderInterface {
    $this->logout_request_signed = $sign_logout_request;
    return $this;
  }

  public function shouldSignLogoutResponse(): bool {
    return (bool) $this->logout_response_signed;
  }

  public function setSignLogoutResponse(bool $sign_logout_response): IdentityProviderInterface {
    $this->logout_response_signed = $sign_logout_response;
    return $this;
  }

  public function wantsSignedMessages(): bool {
    return (bool) $this->want_messages_signed;
  }

  public function setSignedMessages(bool $signed_messages): IdentityProviderInterface {
    $this->want_messages_signed = $signed_messages;
    return $this;
  }

  public function wantsEncryptedAssertions(): bool {
    return (bool) $this->want_assertions_encrypted;
  }

  public function setEncryptedAssertions(bool $encrypted_assertions): IdentityProviderInterface {
    $this->want_assertions_encrypted = $encrypted_assertions;
    return $this;
  }

  public function wantsSignedAssertions(): bool {
    return (bool) $this->want_assertions_signed;
  }

  public function setSignedAssertions(bool $signed_assertions): IdentityProviderInterface {
    $this->want_assertions_signed = $signed_assertions;
    return $this;
  }

  public function wantsEncryptedNameId(): bool {
    return (bool) $this->want_name_id_encrypted;
  }

  public function setEncryptedNameId(bool $encrypted_name_id): IdentityProviderInterface {
    $this->want_name_id_encrypted = $encrypted_name_id;
    return $this;
  }

  public function getAuthnContext(): ?string {
    return $this->authn_context;
  }

  public function setAuthnContext(string $authn_context): IdentityProviderInterface {
    $this->authn_context = $authn_context;
    return $this;
  }

  public function getSignatureAlgorithm(): ?string {
    return $this->signature_algorithm;
  }

  public function setSignatureAlgorithm(string $signature_algorithm): IdentityProviderInterface {
    $this->signature_algorithm = $signature_algorithm;
    return $this;
  }

  public function getDigestAlgorithm(): ?string {
    return $this->digest_algorithm;
  }

  public function setDigestAlgorithm(string $digest_algorithm): IdentityProviderInterface {
    $this->digest_algorithm = $digest_algorithm;
    return $this;
  }

  public function isAdfsCompatible(): bool {
    return (bool) $this->adfs;
  }

  public function setAdfsCompatible(bool $is_adfs_compatible): IdentityProviderInterface {
    $this->adfs = $is_adfs_compatible;
    return $this;
  }

  public function toSettingsArray(): array {
    return [
      'sp' => [
        'assertionConsumerService' => [
          'url' => Url::fromRoute(
            'saml.consume',
            ['identity_provider' => $this->id()],
            ['absolute' => TRUE]
          )->toString(),
        ],
        'singleLogoutService' => [
          'url' => Url::fromRoute(
            'saml.logout',
            ['identity_provider' => $this->id()],
            ['absolute' => TRUE]
          )->toString(),
        ],
        'NameIDFormat' => $this->getNameIdFormat(),
      ],
      'idp' => [
        'entityId' => $this->getEntityId(),
        'singleSignOnService' => [
          'url' => $this->getSsoUrl(),
          'binding' => $this->getSsoBinding(),
        ],
        'singleLogoutService' => [
          'url' => $this->getSloRequestUrl(),
          'responseUrl' => $this->getSloResponseUrl(),
          'binding' => $this->getSloBinding(),
        ],
        'x509cert' => $this->getX509Certificate(),
      ],
      'security' => [
        'nameIdEncrypted' => $this->shouldEncryptNameId(),
        'authnRequestsSigned' => $this->shouldSignAuthnRequest(),
        'logoutRequestSigned' => $this->shouldSignLogoutRequest(),
        'logoutResponseSigned' => $this->shouldSignLogoutResponse(),
        'wantMessagesSigned' => $this->wantsSignedMessages(),
        'wantAssertionsEncrypted' => $this->wantsEncryptedAssertions(),
        'wantAssertionsSigned' => $this->wantsSignedAssertions(),
        'wantNameIdEncrypted' => $this->wantsEncryptedNameId(),
        'signatureAlgorithm' => $this->getSignatureAlgorithm(),
        'digestAlgorithm' => $this->getDigestAlgorithm(),
        'lowercaseUrlencoding' => $this->isAdfsCompatible(),
      ],
    ];
  }

}

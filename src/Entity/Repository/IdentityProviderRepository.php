<?php

declare(strict_types = 1);

namespace Drupal\saml\Entity\Repository;

class IdentityProviderRepository implements IdentityProviderRepositoryInterface {

  protected $entityTypeManager;

  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  public function find(string $id): ?IdentityProviderInterface {
    return $this->entityTypeManager
      ->getStorage('saml_identity_provider')
      ->load($id);
  }

  public function findAll(): array {
    return $this->entityTypeManager
      ->getStorage('saml_identity_provider')
      ->loadMultiple();
  }

}

<?php

declare(strict_types = 1);

namespace Drupal\saml\Entity\Repository;

interface IdentityProviderRepositoryInterface {

  public function find(string $id): ?IdentityProviderInterface;

  public function findAll(): array;

}

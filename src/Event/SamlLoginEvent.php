<?php

declare(strict_types = 1);

namespace Drupal\saml\Event;

use Drupal\saml\Entity\IdentityProviderInterface;
use Drupal\user\UserInterface;
use Symfony\Component\EventDispatcher\Event;

class SamlLoginEvent extends Event {

  public const NAME = 'saml.login';

  protected $account;

  protected $attributes;

  protected $identityProvider;

  public function __construct(
    UserInterface $account,
    IdentityProviderInterface $identity_provider,
    array $attributes
  ) {
    $this->account = $account;
    $this->identityProvider = $identity_provider;
    $this->attributes = $attributes;
  }

  public function getAccount(): UserInterface {
    return $this->account;
  }

  public function getAttributes(): array {
    return $this->attributes;
  }

  public function getIdentityProvider(): IdentityProviderInterface {
    return $this->identityProvider;
  }

}

<?php

declare(strict_types = 1);

namespace Drupal\saml\Event;

use Drupal\saml\Entity\IdentityProviderInterface;
use Drupal\user\UserInterface;
use Symfony\Component\EventDispatcher\Event;

class SamlFindAccountEvent extends Event {

  public const NAME = 'saml.find_account';

  protected $account = NULL;

  protected $attributes;

  protected $nameId;

  protected $identityProvider;

  public function __construct(
    string $name_id,
    IdentityProviderInterface $identity_provider,
    array $attributes
  ) {
    $this->nameId = $name_id;
    $this->idenityProvider = $identity_provider;
    $this->attributes = $attributes;
  }

  public function getAttributes(): array {
    return $this->attributes;
  }

  public function getNameId(): string {
    return $this->nameId;
  }

  public function getIdentityProvider(): IdentityProviderInterface {
    return $this->identityProvider;
  }

  public function getAccount(): ?UserInterface {
    return $this->account;
  }

  public function setAccount(UserInterface $account): self {
    $this->account = $account;
    return $this;
  }

}

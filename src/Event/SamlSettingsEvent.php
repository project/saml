<?php

declare(strict_types = 1);

namespace Drupal\saml\Event;

use Drupal\saml\Config\SamlSettingsInterface;
use Symfony\Component\EventDispatcher\Event;

class SamlSettingsEvent extends Event {

  public const NAME = 'saml.settings';

  protected $settings;

  protected $identityProvider;

  public function __construct(array $settings) {
    $this->settings = $settings;
  }

  public function getSettings(): array {
    return $this->settings;
  }

  public function setSettings(array $settings): self {
    $this->settings = $settings;
    return $this;
  }

  public function getSetting(string $key) {
    return $this->setting[$key] ?? NULL;
  }

  public function setSetting(string $key, $value): self {
    $this->settings[$key] = $value;
    return $this;
  }

  public function getIdentityProvider(): IdentityProviderInterface {
    return $this->identityProvider;
  }

}

<?php

declare(strict_types = 1);

namespace Drupal\saml\Saml2;

use Drupal\saml\Entity\IdentityProviderInterface;
use OneLogin\Saml2\Auth;

interface AuthFactoryInterface {

  public function get(IdentityProviderInterface $identity_provider): Auth;

}

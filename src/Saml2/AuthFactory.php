<?php

declare(strict_types = 1);

namespace Drupal\saml\Saml2;

use Drupal\saml\Config\SamlSettingsInterface;
use Drupal\saml\Entity\IdentityProviderInterface;
use Drupal\saml\Event\SamlSettingsEvent;
use OneLogin\Saml2\Auth;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class AuthFactory implements AuthFactoryInterface {

  protected $entityDispatcher;

  protected $settings;

  protected $initiated;

  public function __construct(
    EventDispatcherInterface $event_dispatcher,
    SamlSettingsInterface $settings
  ) {
    $this->eventDispatcher = $event_dispatcher;
    $this->settings = $settings;
  }

  public function get(IdentityProviderInterface $identity_provider): Auth {
    if (empty($this->initiated[$identity_provider->id()])) {
      $settings = $this->settings->toArray();
      $settings = array_replace_recursive($settings, $identity_provider->toSettingsArray());

      $event = new SamlSettingsEvent($settings, $identity_provider);
      $this->eventDispatcher->dispatch(SamlSettingsEvent::NAME, $event);

      $this->initiated[$identity_provider->id()] = new Auth($event->getSettings());
    }

    return $this->initiated[$identity_provider->id()];
  }

}

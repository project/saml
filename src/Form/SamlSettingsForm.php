<?php

declare(strict_types = 1);

namespace Drupal\saml\Form;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\saml\Config\SamlSettingsInterface;
use Drupal\saml\Saml;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SamlSettingsForm extends ConfigFormBase implements ContainerInjectionInterface {

  protected $settings;

  public static function create(ContainerInterface $container): self {
    return new static ($container->get('saml.settings'));
  }

  public function __construct(SamlSettingsInterface $settings) {
    $this->settings = $settings;
  }

  public function getFormId(): string {
    return 'saml_settings_form';
  }

  public function getEditableConfigNames(): array {
    return ['saml.settings'];
  }

  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['service_provider'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Service Provider'),
    ];

    $form['service_provider']['details'] = [
      '#markup' => $this->t('The service provider is this Drupal application. Settings here will affect all Identity Provider (the external service used to authenticate users).'),
    ];

    $form['service_provider']['entity_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Entity ID'),
      '#description' => $this->t('The Entity ID is the unique identifier for your Drupal application. Give this value to your identity provider.'),
      '#default_value' => $this->settings->getEntityId(),
      '#attributes' => ['readonly' => TRUE],
    ];

    $form['service_provider']['x509_certificate'] = [
      '#type' => 'textarea',
      '#title' => $this->t('x509 Certificate'),
      '#description' => $this->t('Enter the x509 certificate for the Drupal application. This value is a public key and is safe to save as plain text.'),
      '#default_value' => $this->settings->getX509Certificate(),
      '#required' => TRUE,
    ];

    $form['service_provider']['private_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Private Key'),
      '#description' => $this->t('The location of the private key on the file system relative to the <a href="https://api.drupal.org/api/drupal/core%21includes%21bootstrap.inc/constant/DRUPAL_ROOT/9.0.x" target="_blank"><code>DRUPAL_ROOT</code></a>. <strong>Do not add the contents of the private key in this field or store your private key any where in the Drupal root.</strong>'),
      '#default_value' => $this->settings->getPrivateKeyPath(),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $form_state->cleanValues();
    $values = $form_state->getValues();

    $this->settings
      ->setX509Certificate($values['x509_certificate'])
      ->setPrivateKeyPath($values['private_key']);
  }

}

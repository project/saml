<?php

declare(strict_types = 1);

namespace Drupal\saml\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\saml\Entity\IdentityProvider;
use Drupal\saml\Saml;

class IdentityProviderForm extends EntityForm {

  public function form(array $form, FormStateInterface $form_state): array {
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => t('Label'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#description' => $this->t('Provide a label for this Identity Provider to help identify it in the administration pages.'),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => 'Drupal\saml\Entity\IdentityProvider::load',
      ],
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
    ];

    $form['identity_provider'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Identity Provider'),
    ];

    $form['identity_provider']['entity_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Entity ID'),
      '#description' => $this->t('The unique identifier supplied by the Identity Provider.'),
      '#default_value' => $this->entity->getEntityId(),
      '#required' => TRUE,
    ];

    $form['identity_provider']['email_attribute'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email address attribute'),
      '#description' => $this->t('This optional setting enabled a custom attribute to be mapped to the mail field on the User entity.'),
      '#default_value' => $this->entity->getEmailAttributeName(),
    ];

    $form['identity_provider']['name_id_format'] = [
      '#type' => 'select',
      '#title' => $this->t('Name ID format'),
      '#description' => $this->t('The Name ID Format selected here will be used match against the one sent from the Identity Provider.'),
      '#default_value' => $this->entity->getNameIdFormat(),
      '#options' => Saml::getNameIdFormats(),
      '#required' => TRUE,
    ];

    $form['identity_provider']['x509_certificate'] = [
      '#type' => 'textarea',
      '#title' => $this->t('x509 certificate'),
      '#description' => $this->t('Enter the x509 certificate provided by the Identity Provider. This value is a public key and is safe to save as plain text.'),
      '#default_value' => $this->entity->getX509Certificate(),
      '#required' => TRUE,
    ];

    $form['single_sign_on'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Sign Sign On (SSO)'),
    ];

    $form['single_sign_on']['sso_url'] = [
      '#type' => 'url',
      '#title' => $this->t('URL'),
      '#description' => $this->t('The SSO URL supplied by the Identity Provider.'),
      '#default_value' => $this->entity->getSsoUrl(),
      '#required' => TRUE,
    ];

    $form['single_sign_on']['sso_binding'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Binding'),
      '#description' => $this->t('The SAML protocol binding to be used when returning the Response message from the Identity Provider. <em>Note: Currently only the HTTP-Redirect binding is supported.</em>'),
      '#default_value' => $this->entity->getSsoBinding(),
      '#attributes' => ['readonly' => TRUE],
    ];

    $form['single_logout'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Single Logout (SLO)'),
    ];

    $form['single_logout']['slo_request_url'] = [
      '#type' => 'url',
      '#title' => $this->t('Request URL'),
      '#description' => $this->t('The SLO request URL supplied by the Identity Provider.'),
      '#default_value' => $this->entity->getSloRequestUrl(),
    ];

    $form['single_logout']['slo_response_url'] = [
      '#type' => 'url',
      '#title' => $this->t('Response URL'),
      '#description' => $this->t('The SLO response URL supplied by the Identity Provider. <em>Node: If no value is set the SLO request URL will be used.</em>'),
      '#default_value' => $this->entity->getSloResponseUrl(),
    ];

    $form['single_logout']['slo_binding'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Binding'),
      '#description' => $this->t('The SAML protocol binding to be used when returning the LogoutResponse message from the Identity Provider. <em>Note: Currently only the HTTP-Redirect binding is supported.</em>'),
      '#default_value' => $this->entity->getSloBinding(),
      '#attributes' => ['readonly' => TRUE],
    ];

    $form['security'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Security'),
    ];

    $form['security']['authn_context'] = [
      '#type' => 'select',
      '#title' => $this->t('Authn Content'),
      '#description' => $this->t('Select the AuthnContext to be sent in the AuthnRequest.'),
      '#default_value' => $this->entity->getAuthnContext(),
      '#options' => Saml::getAuthnContexts(),
      '#required' => TRUE,
    ];

    $form['security']['digest_algorithm'] = [
      '#type' => 'select',
      '#title' => $this->t('Encryption algorithm'),
      '#description' => $this->t('The algorithm that will be used when consuming a message.'),
      '#default_value' => $this->entity->getDigestAlgorithm(),
      '#options' => Saml::getDigestAlgorithms(),
      '#empty_option' => $this->t('None'),
    ];

    $form['security']['name_id_encrypted'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Encryped the Name ID attribute in the Authn Request'),
      '#description' => $this->t('Enabling this option will encrypt the NameID attribute for Single Logout Requests'),
      '#default_value' => $this->entity->shouldEncryptNameId(),
      '#states' => [
        'visible' => [
          'select[name="digest_algorithm"]' => ['!value' => ''],
        ],
      ],
    ];

    $form['security']['want_name_id_encrypted'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Require the Name ID attribute to be encrypted in the Response'),
      '#description' => $this->t('Enabling this option will require the NameID element in the message to be encrypted. <em>Note: You will need to provide the Identity Provider your x509 certificate.</em>'),
      '#default_value' => $this->entity->wantsEncryptedNameId(),
      '#states' => [
        'visible' => [
          'select[name="digest_algorithm"]' => ['!value' => ''],
        ],
      ],
    ];

    $form['security']['want_assertions_encrypted'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Require assertions to be encrypted in the Response'),
      '#description' => $this->t('Enabling this option will require all assertions in SAML messages to be encrypted. <em>Note: You will need to provide the Identity Provider your x509 certificate.</em>'),
      '#default_value' => $this->entity->wantsEncryptedAssertions(),
      '#states' => [
        'visible' => [
          'select[name="digest_algorithm"]' => ['!value' => ''],
        ],
      ],
    ];

    $form['security']['signature_algorithm'] = [
      '#type' => 'select',
      '#title' => $this->t('Signature Algorithm'),
      '#description' => $this->t('Select the signature algorithm to use with the signing process when sending requests.'),
      '#default_value' => $this->entity->getSignatureAlgorithm(),
      '#options' => Saml::getSignatureAlgorithms(),
      '#empty_option' => $this->t('None'),
    ];

    $form['security']['authn_request_signed'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Sign the Authn Request'),
      '#description' => $this->t('Enabling this options will sign the AuthnRequest with the x509 certificate of this application.'),
      '#default_value' => $this->entity->shouldSignAuthnRequest(),
      '#states' => [
        'visible' => [
          'select[name="signature_algorithm"]' => ['!value' => ''],
        ],
      ],
    ];

    $form['security']['want_messages_signed'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Require the Response to be signed'),
      '#description' => $this->t('Enabling this option will request all SAML messages to be signed.'),
      '#default_value' => $this->entity->wantsSignedMessages(),
      '#states' => [
        'visible' => [
          'select[name="signature_algorithm"]' => ['!value' => ''],
        ],
      ],
    ];

    $form['security']['want_assertions_signed'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Require assertions to be signed in the Response'),
      '#description' => $this->t('Enabling this option will require all assertions to be signed.'),
      '#default_value' => $this->entity->wantsSignedAssertions(),
      '#states' => [
        'visible' => [
          'select[name="signature_algorithm"]' => ['!value' => ''],
        ],
      ],
    ];

    $form['security']['logout_request_signed'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Sign the Logout Request'),
      '#description' => $this->t('Enabling this option will sign the LogoutRequest with the x509 certificate of this application.'),
      '#default_value' => $this->entity->shouldSignLogoutRequest(),
      '#states' => [
        'visible' => [
          'select[name="signature_algorithm"]' => ['!value' => ''],
        ],
      ],
    ];

    $form['security']['logout_response_signed'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Require the Logout Response to be signed'),
      '#description' => $this->t('Enabling this option will sign the LogoutResponse with the x509 certificate of this application.'),
      '#default_value' => $this->entity->shouldSignLogoutResponse(),
      '#states' => [
        'visible' => [
          'select[name="signature_algorithm"]' => ['!value' => ''],
        ],
      ],
    ];

    $form['advanced'] = [
      '#type' => 'details',
      '#title' => $this->t('Advanced'),
    ];

    $form['advanced']['adfs'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable ADFS compatibility mode'),
      '#description' => $this->t('ADFS URL-Encodes SAML data as lowercase, and the module by default uses uppercase. Enable this option for ADFS compatibility on signature verification.'),
      '#default_value' => $this->entity->isAdfsCompatible(),
    ];

    return parent::form($form, $form_state);
  }

  // public function save(array $form, FormStateInterface $form_state): void {
  //   $values = $form_state->getValues();

  //   $this->entity
  //     ->
  // }

}

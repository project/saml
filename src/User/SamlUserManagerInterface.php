<?php

declare(strict_types = 1);

namespace Drupal\saml\User;

use Drupal\saml\Entity\IdentityProviderInterface;
use Drupal\user\UserInterface;

interface SamlUserManagerInterface {

  public function loginRegister(
    string $name_id,
    IdentityProviderInterface $identity_provider,
    array $attributes
  ): UserInterface;

}

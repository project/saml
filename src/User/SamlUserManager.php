<?php

declare(strict_types = 1);

namespace Drupal\saml\User;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\saml\Entity\IdentityProviderInterface;
use Drupal\saml\Event\SamlFindAccountEvent;
use Drupal\saml\Event\SamlLoginEvent;
use Drupal\user\UserInterface;
use OneLogin\Saml2\Auth;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class SamlUserManager implements SamlUserManagerInterface {

  protected $entityTypeManager;

  protected $entityDispatcher;

  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    EventDispatcherInterface $event_dispatcher
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->eventDispatcher = $event_dispatcher;
  }

  public function loginRegister(
    string $name_id,
    IdentityProviderInterface $identity_provider,
    array $attributes
  ): UserInterface {
    $storage = $this->entityTypeManager->getStorage('user');

    $event = new SamlFindAccountEvent($name_id, $identity_provider, $attributes);
    $this->eventDispatcher->dispatch(SamlFindAccountEvent::NAME, $event);
    $account = $event->getAccount();

    if (!$account instanceof UserInterface) {
      $username = $this->cleanUsername($name_id);
      $accounts = $storage->loadByProperties(['name' => $username]);

      if (empty($accounts)) {
        $account = $storage->create([
          'name' => $username,
          'password' => user_password(32),
          'status' => 1,
        ]);
      }
      else {
        $account = reset($accounts);
      }
    }

    $event = new SamlLoginEvent($account, $identity_provider, $attributes);
    $this->eventDispatcher->dispatch(SamlLoginEvent::NAME, $event);

    $account->save();

    user_login_finalize($account);

    return $account;
  }

  private function cleanUsername(string $username): string {
    $username = preg_replace('/[^\w]+/', '_', $username);
    $username = strtolower($username);

    return $username;
  }

}
